const express = require('express')

const app =express()
const customers=require("./Models/customers.js");
const  mongoose  = require('mongoose');
require ('dotenv/config');
mongoose.set("strictQuery", false);
mongoose.connect('mongodb://127.0.0.1:27017/customer',()=>{
    console.log('connected to db ')
})

async function add(){
    try{
        const cst=await customers.create(
            {
                name:'aymane',
                age:40
            });
        await cst.save();
        console.log('added',cst);
    }catch(err){
        console.log(err.message)
    }
}

async function findbyid(){
    try{
        const cst = await customers.findById('63f08614e056ca8960228a3cjjjj')
        console.log('found' , cst);
    }
    catch(err){
        console.log(err.msg)
    }
    
}

async function find(){
    const cst = await customers.find()
    console.log('found' , cst);
}

async function Updatebyid(){
    const cst = await customers.findById('63f08614e056ca8960228a3c')
    console.log('found' , cst);
    cst.name='said'
    cst.age=32
    cst.save()
    console.log('updated' , cst);
}
deleteOne()
async function deleteOne(){
    try{
        const cst = await customers.deleteOne({_id : "63f086086ef5f98083ef6669"})
        console.log('deleted' , cst);
    }
    catch(err){
        console.log(err.msg)
    }
    
}
app.listen(process.env.PORT);